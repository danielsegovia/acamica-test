#!/bin/bash 

ssh ec2-user@18.231.153.181 "cd /home/ec2-user/acamica-test && git pull && npm install && npm run build && pm2 delete acamica-test && pm2 start dist/app.js --watch dist/app.js --name acamica-test"

#rsync -uvrz --exclude '.git' --exclude '.gitlab-ci.yml' --exclude 'system' --exclude 'tmp' --exclude 'uploads' --exclude 'application/config/config.php' --exclude 'application/config/database.php' --exclude '.htaccess'  $CI_PROJECT_DIR/ $LOCAL_PATH.