# Acamica Test

## Configuration
npm install

rename .env.sample to .env

config 
- PORT
- MySQL Credentials

Import sql/acamica-test.sql in your MySQL o MariaDB

## Start APP

###### Dev
npm run dev

###### Production
npm run build

pm2 start dist/app.js

## Front End
rename public/assets/js/base.sample.js to public/assets/js/base.js and config base_url variable

http://localhost:{port}/

## Administrator section

http://localhost:{port}/login.html



Sample credentials

user: admin@admin.com

pass: Acamica-999

