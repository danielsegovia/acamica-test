import Database, { SELECT, INSERT } from "../config/database";

class Career_status {

  static async getAll() {
    
    const rows = await Database.query(
      'SELECT id, name FROM career_status ORDER BY name ASC',
      {
        type: SELECT
      }
    );
    return rows;
  }
}

export default Career_status;
