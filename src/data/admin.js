import Database, { SELECT, INSERT, UPDATE } from "../config/database";

class Admin {

  static async getUserFullInfo(user_id) {

    let query = `SELECT 
        u.id, u.firstname, u.lastname, u.email, u.birthday, u.phone, u.registered,
        c.id city_id, c.name city_name,
        co.id country_id, co.name country_name,
        ca.name career_name, ca.id career_id,
        cs.name career_status, cs.id career_status_id,
        p.name payment_method_name, p.id payment_method_id,
        i.name installment_name, i.id installment_id,
        uc.id user_career_id
      FROM 
        users u 
      INNER JOIN cities c ON u.city_id = c.id
      INNER JOIN countries co ON c.country_id = co.id
      INNER JOIN user_careers uc ON uc.user_id = u.id
      INNER JOIN careers ca ON uc.career_id = ca.id
      INNER JOIN career_status cs ON uc.career_status_id = cs.id
      INNER JOIN installments i ON uc.installment_id = i.id
      INNER JOIN payment_methods p ON uc.payment_method_id = p.id `
      
      
      if(user_id !== false){
        query += `WHERE u.id = ${parseInt(user_id)} `
      }

      query += `ORDER BY u.id DESC`

      const rows = await Database.query(query, {type: SELECT});
      
      return rows;
  }

  static async getUsers(user_id) {

    let query = `SELECT 
        u.id, u.firstname, u.lastname, u.email, u.birthday, u.phone, u.registered,
        c.id city_id, c.name city_name,
        co.id country_id, co.name country_name,
        count(*) as total_careers
      FROM 
        users u 
      INNER JOIN cities c ON u.city_id = c.id
      INNER JOIN countries co ON c.country_id = co.id
      INNER JOIN user_careers uc ON uc.user_id = u.id 
      GROUP BY u.id `
      
      
      if(user_id !== false){
        query += `WHERE u.id = ${parseInt(user_id)} `
      }

      query += `ORDER BY u.id DESC`

      const rows = await Database.query(query, {type: SELECT});
      
      return rows;
  }

  static async usersUpdate(user_id, data) {

    const {
      firstname,
      lastname,
      email,
      day,
      month,
      year,
      phone,
      city_id,
      career_id,
      payment_method_id,
      installment_id,
      career_status_id,
      user_career_id

    } = data;

    const birthday = `${year}-${month}-${day}`

    const transaction = await Database.transaction(async transaction => {
      try {
        
        const user = await Database.query(
         "UPDATE users set firstname=?, lastname=?, email=?, birthday=?, phone=?, city_id=? WHERE id=?",
            {
              replacements: [firstname, lastname, email, birthday, phone, city_id, user_id],  
              type: UPDATE
            }
        )

        await Database.query(
          "UPDATE user_careers SET career_id=?, payment_method_id=?, installment_id=?, career_status_id=? WHERE id=?",
          {
            replacements: [career_id, payment_method_id, installment_id, career_status_id, user_career_id],  
            type: UPDATE
          }
        )
        return true;
      } catch (error) {
        transaction.rollback();
        throw `TRANSACTION_ERROR`;
      }
    }).catch((e) =>{
      return false;
    })

    return transaction;

  }

  
}

export default Admin;
