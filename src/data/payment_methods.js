import Database, { SELECT, INSERT } from "../config/database";

class Payment_methods {

  static async getAll() {
    
    const rows = await Database.query(
      'SELECT id, name FROM payment_methods ORDER BY name ASC',
      {
        type: SELECT
      }
    );
    return rows;
  }
}

export default Payment_methods;
