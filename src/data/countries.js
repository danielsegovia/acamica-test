import Database, { SELECT, INSERT } from "../config/database";

class Countries {

  static async getAll() {
    
    const rows = await Database.query(
      "SELECT id, name FROM countries ORDER BY name ASC",
      {
        type: SELECT
      }
    );
    return rows;
  }
}

export default Countries;
