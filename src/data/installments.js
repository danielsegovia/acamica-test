import Database, { SELECT, INSERT } from "../config/database";

class Installments {

  static async getAll() {
    
    const rows = await Database.query(
      'SELECT id, name FROM installments ORDER BY name ASC',
      {
        type: SELECT
      }
    );
    return rows;
  }
}

export default Installments;
