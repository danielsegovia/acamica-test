import Database, { SELECT, INSERT } from "../config/database";
import bcrypt from "bcrypt"

class Login {

  static async validate(email, password) {
    
    const user = await Database.query(
      'SELECT id, firstname, lastname, email, password, admin FROM users WHERE email = ? LIMIT 1',
      {
        replacements: [email],  
        type: SELECT
      }
    );

    if(user.length === 1){
        var result = await bcrypt.compare(password, user[0].password);
    }

    if(result){
      return user[0]
    }else{
      return false;
    }

  }
}

export default Login;
