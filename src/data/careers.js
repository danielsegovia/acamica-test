import Database, { SELECT, INSERT } from "../config/database";

class Careers {

  static async getAll() {
    
    const rows = await Database.query(
      "SELECT id, name, description FROM careers ORDER BY name ASC",
      {
        type: SELECT
      }
    );
    return rows;
  }
}

export default Careers;
