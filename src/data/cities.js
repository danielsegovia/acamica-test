import Database, { SELECT, INSERT } from "../config/database";

class Cities {

  static async getAll() {
    
    const rows = await Database.query(
        "SELECT * FROM cities",
        {
            type: SELECT
        }
    );
    return rows;
  }

  static async getByCountry(country_id) {
    
    const rows = await Database.query(
      "SELECT id, country_id, name FROM cities WHERE country_id = ? ORDER BY name ASC",
      {
        replacements: [country_id],  
        type: SELECT
      }
    );
    return rows;
  }
}

export default Cities;
