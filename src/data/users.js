import Database, { SELECT, INSERT } from "../config/database";

class Users {

  static async insert(data) {

      const {
        firstname,
        lastname,
        email,
        day,
        month,
        year,
        phone,
        city_id,
        career_id,
        payment_method_id,
        installment_id
      } = data;

      const birthday = `${year}-${month}-${day}`

      const transaction = await Database.transaction(async transaction => {
      try {
        
        const user = await Database.query(
         "INSERT INTO users (firstname, lastname, email, birthday, phone, city_id, registered, admin, `password`) VALUES (?, ?, ?, ?, ?, ?, NOW(), 0, '')",
            {
              replacements: [firstname, lastname, email, birthday, phone, city_id],  
              type: INSERT
            }
        )

        const career = await Database.query(
          "INSERT INTO user_careers (user_id, career_id, payment_method_id, installment_id, registered) VALUES (?, ?, ?, ?, NOW())",
          {
            replacements: [user[0], career_id, payment_method_id, installment_id],  
            type: INSERT
          }
        )
        return true;
      } catch (error) {
        transaction.rollback();
        throw `TRANSACTION_ERROR`;
      }
    }).catch((e) =>{
      return false;
    })

    return transaction;
    
  }
}

export default Users;
