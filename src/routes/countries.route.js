import { Router } from "express";
import Countries from "../data/countries";

const route = Router();

export default app => {
    app.use("/countries", route);

    route.get("/", async (req, res) => {
		const countries = await Countries.getAll();
    	res.status(200).json({ status: 200, message: "Countries listed successfully", countries: countries });
    });

};