import { Router } from "express";
import Installments from "../data/installments";


const route = Router();

export default app => {
      app.use("/installments", route);

      route.get("/", async (req, res) => {
        const installments = await Installments.getAll();
        res.status(200).json({ status: 200, message: "Installments listed successfully", installments: installments });
    });

};