import { Router } from "express";
import Careers from "../data/careers";

const route = Router();

export default app => {
      app.use("/careers", route);

      route.get("/", async (req, res) => {
        const careers = await Careers.getAll();
        res.status(200).json({ status: 200, message: "careers listed successfully", careers: careers });
      });

};