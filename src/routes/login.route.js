import { Router } from "express";
import Login from "../data/login";
import jwt from "jsonwebtoken";

const route = Router();

export default app => {
    app.use("/login", route);

    route.post("/", async (req, res) => {
        
        const {
            email,
            password,
        } = req.body;

        //check user & pass in database
        const login = await Login.validate(email, password);

        if(login !== false){
            const user = {id: login['id'], firstname: login['firstname'], lastname: login['lastname'], email: login['email'], admin: login['admin']}
            const access_token = jwt.sign(user, process.env.JTW_ACCESS_SECRET);
            res.status(200).json({ status: 200, message: "Authentication OK", access_token: access_token});

        }else{
            res.status(401).json({ status: 401, message: "Authentication fail"});
        }
    });
};