import { Router } from "express";
import Payment_methods from "../data/payment_methods";

const route = Router();

export default app => {
    app.use("/payment_methods", route);

    route.get("/", async (req, res) => {
        console.log("estoy");
		const payment_methods = await Payment_methods.getAll();
    	res.status(200).json({ status: 200, message: "Payment methods listed successfully", payment_methods: payment_methods });
    });

};