import { Router } from "express";
import Admin from "../data/admin";
import { authorize_administrator } from "../helper/auth";

const route = Router();

export default app => {
    app.use("/admin", authorize_administrator, route);

    route.post("/", async (req, res) => {
        //check session
        const user = {fullname: req.user.firstname, lastname: req.user.lastname}
        res.status(200).json({ status: 200, message: "valid credentials", fullname: `${req.user.firstname} ${req.user.lastname}` });
    });

    route.get("/users/:id?", async (req, res) => {
        var user_id = (req.params.id)? req.params.id : false;
        const users = await Admin.getUsers(user_id);
        res.status(200).json({ status: 200, message: "Users list", users: users});
    });

    route.get("/getUserFullInfo/:id?", async (req, res) => {
        var user_id = (req.params.id)? req.params.id : false;
        const users = await Admin.getUserFullInfo(user_id);
        res.status(200).json({ status: 200, message: "Users list", users: users});
    });

    route.put("/users/:id?", async (req, res) => {
        if (req.params.id){
            var user_id = req.params.id;
        }else{
            res.status(403).json({ status: 403, message: "No user_id"});
        }

        const users = await Admin.usersUpdate(user_id, req.body);

        res.status(200).json({ status: 200, message: "Users list"});
    });


};