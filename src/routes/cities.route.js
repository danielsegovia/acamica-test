import { Router } from "express";
import Cities from "../data/cities";

const route = Router();

export default app => {
    app.use("/cities", route);
    
    route.get("/", async (req, res) => {
        //console.log(country_id)
		const cities = await Cities.getAll();
    	res.status(200).json({ status: 200, message: "Cities listed successfully", cities: cities });
    });

    route.get("/by_country/:country_id", async (req, res) => {
        const { country_id } = req.params;
		const cities = await Cities.getByCountry(country_id);
    	res.status(200).json({ status: 200, message: "Cities by country listed successfully", cities: cities });
    });

};