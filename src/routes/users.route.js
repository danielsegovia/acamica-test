import { Router } from "express";
import Users from "../data/users";

const route = Router();

export default app => {
    app.use("/users", route);
    
    route.get("/", async (req, res) => {
		const users = await Users.getAll();
    	res.status(200).json({ status: 200, message: "Users listed successfully", data: users });
    });

    route.post("/", async (req, res) => {
        const users = await Users.insert(req.body);
        if(users !== false){
            res.status(200).json({ status: 200, message: "Users listed successfully"});
        }else{
            res.status(401).json({ status: 401, message: "Información incorrecta"});
        }
    });
};