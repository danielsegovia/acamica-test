import { Router } from "express";
import Career_status from "../data/career_status";


const route = Router();

export default app => {
      app.use("/career_status", route);

      route.get("/", async (req, res) => {
        const career_status = await Career_status.getAll();
        res.status(200).json({ status: 200, message: "career_status listed successfully", career_status: career_status });
    });

};