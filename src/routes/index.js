import { Router } from 'express';

 
// import routes and add to export
import installments from './installments.route';
import countries from './countries.route';
import cities from './cities.route';
import users from './users.route';
import careers from './careers.route';
import payment_methods from './payment_methods.route';
import login from './login.route';
import admin from './admin.route';
import career_status from './career_status.route';


export default () => {
	const app = Router();

	installments(app); 
	countries(app); 
	cities(app); 
	users(app); 
	careers(app); 
	payment_methods(app); 
	login(app); 
	admin(app); 
	career_status(app); 

	return app;
};
