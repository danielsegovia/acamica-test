import jwt from "jsonwebtoken";

export const authorize_administrator = (req, res, nex) => {
    const auth = req.headers['authorization'];
    const token = auth && auth.split(' ')[1]

    if(token == null) res.status(403).json({ status: 403, message: "Invalid Token"});

    jwt.verify(token, process.env.JTW_ACCESS_SECRET, (err, user) =>{
        if(err) res.status(403).json({ status: 403, message: "Invalid Token"})

        if(user.admin === 1){
            req.user = user
            nex();
        }else{
            res.status(403).json({ status: 403, message: "Only admins"})
        }

        
    })
}

