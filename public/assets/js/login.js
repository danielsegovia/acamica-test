$(document).ready(() =>{
    $('#login').submit(function (e){
        e.preventDefault();
        $.post( 
            base_url + 'login', 
            $( "#login" ).serialize() 
        ).fail((data)=>{
           $('.invalid-feedback').show();
        }).done((data)=>{
            localStorage.setItem('access_token', data.access_token);
            window.location.replace("./admin/index.html");
        });
    })

    $("input").keypress(function(){
        $('.invalid-feedback').hide();
    });
});

