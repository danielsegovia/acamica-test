const form_option_template = (id, name, selected) => `<option value="${id}" ${selected}>${name}</option>`                                

$(document).ready(() =>{

    const user_id = window.location.href.split('#')[1]

    $.ajax({
        url: base_url + "admin/getUserFullInfo/" + user_id,
        type: 'GET',
        headers: {"Authorization": 'Bearer ' + localStorage.getItem('access_token')}
    }).fail(()=>{
        window.location.replace("../login.html");
    }).done((data) => {

        //Load information in form
        var user = data.users[0];

        $('#firstname').val(user.firstname);
        $('#lastname').val(user.lastname);
        $('#email').val(user.email);
        $('#phone').val(user.phone);
        const birthday = user.birthday.split('-')
        $('#day').val(birthday[2]);
        $('#month').val(birthday[1]);
        $('#year').val(birthday[0]);
        $('#user_career_id').val(user.user_career_id);
        

        $.getJSON(base_url + 'careers', function(data) {
            $.map(data.careers, function(career, key) {
                var selected = (career.id === user.career_id)? "selected" : "";
                $('#career_id').append(form_option_template(career.id, career.name, selected));
            });
        }).fail(()=> $('#careers').html("Lo sentimos. No hemos podido cargar la lista de carreras disponibles") );

        $.getJSON(base_url + 'countries', function(data) {
            $.map(data.countries, function(country, key) {
                var selected = (country.id === user.country_id)? "selected" : "";
                $('#country_id').append(form_option_template(country.id, country.name, selected));
            });
            
        }).fail(()=> console.log("No fue posible cargar la lista de países") );

        $.getJSON(base_url + 'cities/by_country/' + user.country_id, function(data) {
            $('#city_id').html('<option value="">Selecciona tu ciudad</option>');
            $.map(data.cities, function(city, key) {
                var selected = (city.id === user.city_id)? "selected" : "";
                $('#city_id').append(form_option_template(city.id, city.name, selected));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de ciudades"));

        $.getJSON(base_url + 'payment_methods', function(data) {
            $.map(data.payment_methods, function(payment_method, key) {
                var selected = (payment_method.id === user.payment_method_id)? "selected" : "";
                $('#payment_method_id').append(form_option_template(payment_method.id, payment_method.name, selected));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de los métodos de pago") );

        $.getJSON(base_url + 'installments', function(data) {
            $.map(data.installments, function(installment, key) {
                var selected = (installment.id === user.installment_id)? "selected" : "";
                $('#installment_id').append(form_option_template(installment.id, installment.name, selected));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de coutas") );

        $.getJSON(base_url + 'career_status', function(data) {
            $.map(data.career_status, function(status, key) {
                var selected = (status.id === user.career_status_id)? "selected" : "";
                $('#career_status_id').append(form_option_template(status.id, status.name, selected));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de coutas") );

    });


    


    $('#update').submit(function (e){
        e.preventDefault();
        $.ajax({
            url: base_url + "admin/users/" + user_id,
            type: 'PUT',
            headers: {"Authorization": 'Bearer ' + localStorage.getItem('access_token')},
            data: $( "#update" ).serialize() 
        }).fail(()=>{
            window.location.replace("../login.html");
        }).done((data)=>{
            $('.valid-feedback').show();
            setTimeout(function(){ $('.valid-feedback').hide() }, 2000);
        })
    })

    $('#country_id').change(function (){
        let country_id = $(this).val();
        $.getJSON(base_url + 'cities/by_country/' + country_id, function(data) {
            $('#city_id').html('<option value="">Selecciona tu ciudad</option>');
            $.map(data.cities, function(city, key) {
                $('#city_id').append(form_option_template(city.id, city.name));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de ciudades") );
        
    })

});

