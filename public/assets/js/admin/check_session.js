(async () => {
    //valido la session, si no es valida reidirijo al login
    $.ajax({
        url: base_url + "admin",
        type: 'POST',
        // Fetch the stored token from localStorage and set in the header
        headers: {"Authorization": 'Bearer ' + localStorage.getItem('access_token')}
    }).fail(()=>{
        window.location.replace("../login.html");
    });
})();