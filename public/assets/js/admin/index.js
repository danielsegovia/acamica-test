$(document).ready(() =>{

    const user_tr_template = (user) => `<tr>
        <td>${user.id}</td>
        <td>${user.firstname}</td>
        <td>${user.lastname}</td>
        <td>${user.email}</td>
        <td>${user.total_careers}</td>
        <td>${user.country_name}</td>
        <td>${user.city_name}</td>
        <td><a href="edit.html#${user.id}">Ver más</a></td>
    </tr>`

    $.ajax({
        url: base_url + "admin/users",
        type: 'GET',
        // Fetch the stored token from localStorage and set in the header
        headers: {"Authorization": 'Bearer ' + localStorage.getItem('access_token')}
    }).fail(()=>{
        window.location.replace("../login.html");
    }).done((data) => {
        $.map(data.users, function(user, key) {
            $('#user_data').append(user_tr_template(user));
        });
    });
});

