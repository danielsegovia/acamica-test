const career_card_template = (id, name, description) => `<div class="col-12 col-md-6 col-lg-6 mb-4">
                                    <div class="card h-100">
                                        
                                        <div class="card-body">
                                            <h4>${name}</h4>
                                            <p class="card-text">${description}</p>
                                            <button type="button" class="btn btn-primary register" data-career-id="${id}">Registrate</button>
                                        </div>
                                    </div>
                                </div>`

const form_option_template = (id, name) => `<option value="${id}">${name}</option>`                                

$(document).ready(() =>{
    $.getJSON(base_url + 'careers', function(data) {
        $.map(data.careers, function(career, key) {
            $('#careers').append(career_card_template(career.id, career.name, career.description));
            $('#career_id').append(form_option_template(career.id, career.name));
        });
    }).fail(()=> $('#careers').html("Lo sentimos. No hemos podido cargar la lista de carreras disponibles") );

    $.getJSON(base_url + 'countries', function(data) {
        $.map(data.countries, function(country, key) {
            $('#country_id').append(form_option_template(country.id, country.name));
        });
    }).fail(()=> console.log("No fue posible cargar la lista de países") );

    $('#country_id').change(function (){
        let country_id = $(this).val();
        $.getJSON(base_url + 'cities/by_country/' + country_id, function(data) {
            $('#city_id').html('<option value="">Selecciona tu ciudad</option>');
            $.map(data.cities, function(city, key) {
                $('#city_id').append(form_option_template(city.id, city.name));
            });
        }).fail(()=> console.log("No fue posible cargar la lista de ciudades") );
        
    })

    $.getJSON(base_url + 'payment_methods', function(data) {
        $.map(data.payment_methods, function(payment_method, key) {
            $('#payment_method_id').append(form_option_template(payment_method.id, payment_method.name));
        });
    }).fail(()=> console.log("No fue posible cargar la lista de los métodos de pago") );

    $.getJSON(base_url + 'installments', function(data) {
        $.map(data.installments, function(installment, key) {
            console.log(installment)
            $('#installment_id').append(form_option_template(installment.id, installment.name));
        });
    }).fail(()=> console.log("No fue posible cargar la lista de coutas") );

    $('#register').submit(function (e){
        e.preventDefault();
        const aDate   = moment($('#year').val() + '-' + $('#month').val() + '-' + $('#day').val(), 'YYYY-MM-DD', true);
        if(aDate.isValid()){
        
            $.post( 
                base_url + 'users', 
                $( "#register" ).serialize() 
            ).fail((data)=>{
                $('#register').hide();
                $('#register_msj').html("<b>Ha ocurrido un error</b><br/>Por favor envie un email a hola@acamica-test.com");
                $('#register_msj').removeClass('d-none');
            }).done(()=>{
                $('#register').hide();
                $('#register_msj').html('<b>Muchas gracias por confiar en nosotros</b><br/>En breve lo estaremos contactando para continuar con el&nbsp;proceso');
                $('#register_msj').removeClass('d-none');
            });
        }else{
            $('#err_birthday').show();
        }

    })

    $('.register').click(()=>{
        alert("este")
    })

    $('.birthday').change(()=>{

        if($('#year').val() != '' && $('#month').val() != '' && $('#day').val() != '' ){
            const aDate   = moment($('#year').val() + '-' + $('#month').val() + '-' + $('#day').val(), 'YYYY-MM-DD', true);
            if(aDate.isValid()){
                $('#err_birthday').hide();
            }else{
                $('#err_birthday').show();
            } 
        }
        
    })




});

